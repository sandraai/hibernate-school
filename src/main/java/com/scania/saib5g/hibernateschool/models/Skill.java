package com.scania.saib5g.hibernateschool.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Skill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "description")
    private String description;

    @ManyToMany
    @JoinTable(
            name = "teacher_skill",
            joinColumns = {@JoinColumn(name = "teacher_id")},
            inverseJoinColumns = {@JoinColumn(name = "skill_id")}
    )
    public List<Teacher> teachers;

    @JsonGetter("teachers")
    public List<String> teachersGetter() {
        return teachers.stream()
                .map(teacher -> {
                    return "/api/school/teachers/" + teacher.getId();
                }).collect(Collectors.toList());
    }


    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Teacher> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<Teacher> teachers) {
        this.teachers = teachers;
    }
}