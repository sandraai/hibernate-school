package com.scania.saib5g.hibernateschool.controllers;

import com.scania.saib5g.hibernateschool.models.Skill;
import com.scania.saib5g.hibernateschool.repositories.SkillRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/school/skills")
@Tag(name = "Skills", description = "The API resource for skills")
public class SkillController {
    @Autowired
    private SkillRepository skillRepository;

    @Operation(summary = "Retrieves all skills which currently exist in the school database")
    @GetMapping
    public ResponseEntity<List<Skill>> getAllSkills () {
        return new ResponseEntity<>(skillRepository.findAll(), HttpStatus.OK);
    }

    @Operation(summary = "Retrieves a specific skill by skill id from the school database")
    @GetMapping("/{id}")
    public ResponseEntity<Skill> getSkill (@PathVariable Long id) {
        if (!skillRepository.existsById(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(skillRepository.findById(id).get(), HttpStatus.OK);
    }

    @Operation(summary = "Adds a new skill to the database")
    @PostMapping
    public ResponseEntity<Skill> addSkill (@RequestBody Skill skill) {
        return new ResponseEntity<>(skillRepository.save(skill), HttpStatus.CREATED);
    }

    @Operation(summary = "Updates a specific skill by id in the database")
    @PutMapping("/{id}")
    public ResponseEntity<Skill> updateSkill (@PathVariable Long id, @RequestBody Skill skill) {
        if (id != skill.getId()) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(skillRepository.save(skill), HttpStatus.NO_CONTENT);
    }

    @Operation(summary = "Removes a specific skill by id from the database")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteSkill (@PathVariable Long id) {
        if (!skillRepository.existsById(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        skillRepository.deleteById(id);
        return new ResponseEntity<>("Skill with id " + id + " was deleted.", HttpStatus.OK);
    }
}
