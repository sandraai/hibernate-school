package com.scania.saib5g.hibernateschool.controllers;

import com.scania.saib5g.hibernateschool.models.Teacher;
import com.scania.saib5g.hibernateschool.repositories.TeacherRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/school/teachers")
@Tag(name = "Teachers", description = "The API resource for teachers")
public class TeacherController {

    @Autowired
    private TeacherRepository teacherRepository;

    @Operation(summary = "Retrieves all teachers which currently exist in the school database")
    @GetMapping
    public ResponseEntity<List<Teacher>> getAllTeachers () {
        return new ResponseEntity<>(teacherRepository.findAll(), HttpStatus.OK);
    }

    @Operation(summary = "Retrieves a specific teacher by teacher id from the school database")
    @GetMapping("/{id}")
    public ResponseEntity<Teacher> getTeacher (@PathVariable Long id) {
        if (!teacherRepository.existsById(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(teacherRepository.findById(id).get(), HttpStatus.OK);
    }

    @Operation(summary = "Adds a new teacher to the database")
    @PostMapping
    public ResponseEntity<Teacher> addTeacher (@RequestBody Teacher teacher) {
        return new ResponseEntity<>(teacherRepository.save(teacher), HttpStatus.CREATED);
    }

    @Operation(summary = "Updates a specific teacher by id in the database")
    @PutMapping("/{id}")
    public ResponseEntity<Teacher> updateTeacher (@PathVariable Long id, @RequestBody Teacher teacher) {
        if (id != teacher.getId()) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(teacherRepository.save(teacher), HttpStatus.NO_CONTENT);
    }

    @Operation(summary = "Removes a specific teacher by id from the database")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteTeacher (@PathVariable Long id) {
        if (!teacherRepository.existsById(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        teacherRepository.deleteById(id);
        return new ResponseEntity<>("Teacher with id " + id + " was deleted.", HttpStatus.OK);
    }
}
