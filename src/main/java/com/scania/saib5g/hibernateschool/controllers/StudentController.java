package com.scania.saib5g.hibernateschool.controllers;

import com.scania.saib5g.hibernateschool.models.Student;
import com.scania.saib5g.hibernateschool.repositories.StudentRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/school/students")
@Tag(name = "Students", description = "The API resource for students")
public class StudentController {
    @Autowired
    private StudentRepository studentRepository;

    @Operation(summary = "Retrieves all students which currently exist in the school database")
    @GetMapping
    public ResponseEntity<List<Student>> getAllStudents () {
        return new ResponseEntity<>(studentRepository.findAll(), HttpStatus.OK);
    }

    @Operation(summary = "Retrieves a specific student by student id from the school database")
    @GetMapping("/{id}")
    public ResponseEntity<Student> getStudent (@PathVariable Long id) {
        if (!studentRepository.existsById(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(studentRepository.findById(id).get(), HttpStatus.OK);
    }

    @Operation(summary = "Adds a new student to the database")
    @PostMapping
    public ResponseEntity<Student> addStudent (@RequestBody Student student) {
        return new ResponseEntity<>(studentRepository.save(student), HttpStatus.CREATED);
    }

    @Operation(summary = "Updates a specific student by id in the database")
    @PutMapping("/{id}")
    public ResponseEntity<Student> updateStudent (@PathVariable Long id, @RequestBody Student student) {
        if (id != student.getId()) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(studentRepository.save(student), HttpStatus.NO_CONTENT);
    }

    @Operation(summary = "Removes a specific student by id from the database")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteStudent (@PathVariable Long id) {
        if (!studentRepository.existsById(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        studentRepository.deleteById(id);
        return new ResponseEntity<>("Student with id " + id + " was deleted.", HttpStatus.OK);
    }
}
