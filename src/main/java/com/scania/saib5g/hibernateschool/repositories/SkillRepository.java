package com.scania.saib5g.hibernateschool.repositories;

import com.scania.saib5g.hibernateschool.models.Skill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SkillRepository extends JpaRepository<Skill, Long> {
}
