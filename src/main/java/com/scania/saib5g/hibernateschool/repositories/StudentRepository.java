package com.scania.saib5g.hibernateschool.repositories;

import com.scania.saib5g.hibernateschool.models.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
}
