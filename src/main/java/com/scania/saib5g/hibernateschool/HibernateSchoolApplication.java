package com.scania.saib5g.hibernateschool;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Hibernate School API", version = "1.0", description = "A school system for storing and keeping track of teachers, students and skills"))
public class HibernateSchoolApplication {

    public static void main(String[] args) {
        SpringApplication.run(HibernateSchoolApplication.class, args);
    }

}
